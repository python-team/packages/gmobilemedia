#!/usr/bin/env python
# -*- coding: utf-8 -*-

###
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###

import gtk
import gobject
import user, os, ConfigParser

def query(myObject):
	print myObject.__name__
	print myObject.__doc__
	
	propiedades = [m for m in dir(myObject) if not callable(getattr(myObject,m))]
	print "Properties:"
	print propiedades
	metodos = [m for m in dir(myObject) if callable(getattr(myObject,m))]
	print "Methods:"
	print metodos
	
def getReadableSize(Size):
	value = "0 KB"
	if Size>0:
		unit = float(1024)

		if Size < pow(unit, 2):
			value = ("%s KB") % (round(Size / unit, 2))
		else:
			value = ("%s MB") % (round(Size / pow(unit, 2), 2))

	return value
	
class MessageBox(gtk.Dialog):
	def __init__(self, message="", buttons=(), pixmap=None, modal= True):
		gtk.Dialog.__init__(self)
		self.connect("destroy", self.quit)
		self.connect("delete_event", self.quit)
		if modal:
			self.set_modal(True)
		hbox = gtk.HBox(spacing=10)
		hbox.set_border_width(5)
		self.vbox.pack_start(hbox)
		hbox.show()
		if pixmap:
			self.realize()
			self.set_icon_name(pixmap)
			n_pixmap = gtk.Image()
			n_pixmap.set_from_stock(pixmap, gtk.ICON_SIZE_DIALOG)
			hbox.pack_start(n_pixmap, expand=True)
			n_pixmap.show()
		label = gtk.Label(message)
		hbox.pack_start(label)
		label.show()
		for text in buttons:
			b = self.add_button(text, -1)
			b.set_data("user_data", text)
			b.set_flags(gtk.CAN_DEFAULT)
			b.set_data("user_data", text)
			b.connect("clicked", self.click)
		self.ret = None
	def quit(self, *args):
		self.hide()
		self.destroy()
	def click(self, button):
		self.ret = button.get_data("user_data")
		self.quit()

# create a message box, and return which button was pressed     
def message_box(title="Message Box", message="", buttons=(), pixmap=None, modal= True):
	win = MessageBox(message, buttons, pixmap=pixmap, modal=modal)
	win.set_title(title)
	win.show()
	win.run()
	
	return win.ret

# Progress Dialog 
# --------------------------------------------
# Code taken from Serpentine
#

def hig_label (text = None):
    lbl = gtk.Label ()
    lbl.set_alignment (0, 0)
    if text:
        lbl.set_markup (text)
    lbl.set_selectable (False)
    lbl.set_line_wrap (True)
    return lbl

def hig_dialog (dialog):
	"""
	Creates a gtk.Dialog and adds a gtk.Alignment has it's child.
	Returns the gtk.Alignment instance.
	
	See: http://bugzilla.gnome.org/show_bug.cgi?id=163850
	"""
	vbox = gtk.VBox (False, 24)
	vbox.set_border_width (12)
	vbox.show ()
	dialog.vbox = vbox
	
	hbox = gtk.HButtonBox ()
	hbox.set_spacing (6)
	hbox.set_layout (gtk.BUTONBOX_END)
	hbox.show ()
	vbox.pack_end (hbox)
	return dialog, vbox

class HigProgress (gtk.Window):
	"""
		HigProgress returns a window that contains a number of properties to
		access what a common Progress window should have.
	"""
	def __init__ (self):
		gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)
		self.set_border_width (6)
		self.set_resizable (False)
		self.set_title ('')
		# defaults to center location
		self.set_position (gtk.WIN_POS_CENTER)
		self.set_modal(True)
		self.connect ("delete-event", self.__on_close)

		# main container
		main = gtk.VBox (spacing = 12)
		main.set_spacing (12)
		main.set_border_width (6)
		main.show()
		self.add (main)

		# primary text
		alg = gtk.Alignment ()
		alg.set_padding (0, 6, 0, 0)
		alg.show()
		main.pack_start (alg, False, False)
		lbl = hig_label()
		lbl.set_selectable (False)
		lbl.show()
		self.__primary_label = lbl
		alg.add (lbl)

		# secondary text
		lbl = hig_label()
		lbl.set_selectable (False)
		lbl.show()
		main.pack_start (lbl, False, False)
		self.__secondary_label = lbl

		# Progress bar
		vbox = gtk.VBox()
		vbox.show()
		main.pack_start (vbox, False, False)

		prog = gtk.ProgressBar ()
		prog.show()
		self.__progress_bar = prog
		vbox.pack_start (prog, expand = False)

		lbl = hig_label ()
		lbl.set_selectable (False)
		lbl.show ()
		self.__sub_progress_label = lbl
		vbox.pack_start (lbl, False, False)

		lbl = hig_label ()
		lbl.set_selectable (False)
		lbl.show ()
		self.__sub_progress_label = lbl
		vbox.pack_start (lbl, False, False)

		# Buttons box
		bbox = gtk.HButtonBox ()
		bbox.set_layout (gtk.BUTTONBOX_END)
		bbox.show ()

		# Cancel Button
		cancel = gtk.Button (gtk.STOCK_CANCEL)
		cancel.set_use_stock (True)
		cancel.show ()
		self.__cancel = cancel
		bbox.add (cancel)
		main.add (bbox)

		# Close button, which is hidden by default
		close = gtk.Button (gtk.STOCK_CLOSE)
		close.set_use_stock (True)
		close.hide ()
		bbox.add (close)
		self.__close = close

	primary_label = property (lambda self: self.__primary_label)
	secondary_label = property (lambda self: self.__secondary_label)
	progress_bar = property (lambda self: self.__progress_bar)
	sub_progress_label = property (lambda self: self.__sub_progress_label)
	cancel_button = property (lambda self: self.__cancel)
	close_button = property (lambda self: self.__close)

	def primary_text (self, text):
		self.primary_label.set_markup ('<span weight="bold" size="larger">'+text+'</span>')
		self.set_title (text)

	primary_text = property (fset = primary_text)

	def secondary_text (self, text):
		self.secondary_label.set_markup (text)

	secondary_text = property (fset = secondary_text)

	def progress_fraction (self, fraction):
		self.progress_bar.set_fraction (fraction)

	progress_fraction = property (fset = progress_fraction)

	def progress_text (self, text):
		self.progress_bar.set_text (text)
	progress_text = property (fset = progress_text)

	def sub_progress_text (self, text):
		self.sub_progress_label.set_markup ('<i>'+text+'</i>')
	sub_progress_text = property (fset = sub_progress_text)

	def __on_close (self, *args):
		if not self.cancel_button.get_property ("sensitive"):
			return True
		# click on the cancel button
		self.cancel_button.clicked ()
		# let the clicked event close the window if it likes too
		return True

gobject.type_register (HigProgress)

class AppSettings:
	filename = ""
	dirname = ""
	config = None
	gammu_config = ""
	
	def __init__(self):
		self.dirname = user.home + os.sep + ".gmobilemedia" + os.sep
		self.gammu_config = user.home + os.sep + ".gammurc"
		self.filename = self.dirname + 'default.cfg'
		
		if not os.path.isdir(self.dirname):
		    os.makedirs(self.dirname, mode=0700)
		
		self.config = ConfigParser.ConfigParser()
	
	def is_valid(self):
		if not os.path.exists(self.gammu_config):
			return False
		
		return True
	
	def get_value(self, section, option, default):
		value = default
		
		try:
			value = self.config.get(section, option)
		except ConfigParser.NoSectionError:
			pass
		except ConfigParser.NoOptionError:
			pass
			
		return value
		
	def set_value(self, section, item, value):
		if not self.config.has_section(section):
			self.config.add_section(section)
			
		self.config.set(section, item, value)
		
	def remove_value(self, section, option):
		try:
			self.config.remove_option(section, option)
		except:
			return False
			
		return True
	
	def read(self):
		self.config.read(self.gammu_config)
	
	def save(self):
		f = file(self.gammu_config, 'w')
		self.config.write(f)
		f.close()
