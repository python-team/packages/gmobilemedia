#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
    import gtk.glade
except:
    sys.exit(1)

import configuration
import ConfigParser, os
from engine import *

class wndPreferences:
	
	windowname = 'wndPreferences'
	settings = None
	
	def __init__(self, config = None):
		''' '''
		
		if config!=None:
			self.settings = config
			self.settings.read()
		
		self.wTree = gtk.glade.XML(configuration.GLADE_FILE, self.windowname, configuration.TEXT_DOMAIN)
		self.win = self.wTree.get_widget(self.windowname)
		
		self.model = self.wTree.get_widget("cmbModel")
		self.connection = self.wTree.get_widget("cmbConnection")
		self.port = self.wTree.get_widget("cmbPort")
		self.log_file = self.wTree.get_widget("fileLogFile")
		self.sync = self.wTree.get_widget("ckSynchronize")

		self.win.set_modal(True)

		handlers = {'on_btnOk_clicked': self.on_btnOk_clicked,
		'on_btnCancel_clicked': self.on_btnCancel_clicked
		}
		self.wTree.signal_autoconnect(handlers)
		self.load_settings()
		
	def load_settings(self):
		if self.settings:
			port = self.settings.get_value("gammu", "port", "")
			model = self.settings.get_value("gammu", "model", "")
			connection = self.settings.get_value("gammu", "connection", "at")
			logfile = self.settings.get_value("gammu", "logfile", "")
			synctime = self.settings.get_value("gammu", "synchronizetime", "no")
			
			if synctime == "yes":
				self.sync.set_active(True)
			
			listConnection = gtk.ListStore(str)
			self.connection.set_model(listConnection)
			#~ self.connection.set_text_column(0)
			index = 0
			for conx in configuration.AVAILABLE_CONNECTIONS:
				listConnection.append([conx])
				if conx == connection:
					self.connection.set_active(index)
				index+=1
			
			if os.path.isfile(logfile):
				self.log_file.set_filename(logfile)
			
			listModel = gtk.ListStore(str)
			self.model.set_model(listModel)
			self.model.set_text_column(0)
			listModel.append(['autodetect'])
			self.model.set_active(0)

			if model:
				listModel.append([model])
				self.model.set_active(1)

			liststore = gtk.ListStore(str)
			self.port.set_model(liststore)
			self.port.set_text_column(0)

			counter = 0
			for serial_port in configuration.SERIAL_PORTS:
				liststore.append([serial_port])
				if port == serial_port:
					self.port.set_active(counter)
					port = ""
				counter +=1

			if port:
				liststore.append([port])
				self.port.set_active(counter)
	
	def save_settings(self):
		if self.sync.get_active():
			synchronize = "yes"
		else:
			synchronize = "no"
		self.settings.set_value("gammu", "SynchronizeTime", synchronize)
		
		logfile = self.log_file.get_filename()
		if logfile:
			self.settings.set_value("gammu", "Logfile", logfile)
			self.settings.set_value("gammu", "Logformat", "errors")
		else:
			self.settings.remove_value("gammu", "Logfile")
			self.settings.set_value("gammu", "Logformat", "nothing")
			
		port = self.port.get_active_text()
		if port:
			self.settings.set_value("gammu", "port", port)
			
		model = self.model.get_active_text()
		if model:
			if model == "autodetect":
				self.settings.remove_value("gammu", "Model")
			else:
				self.settings.set_value("gammu", "Model", model)
		
		connection = self.connection.get_active_text()
		if connection:
			self.settings.set_value("gammu", "Connection", connection)
		
		self.settings.save()
			
		return True

	def on_btnCancel_clicked(self, *args):
		self.win.destroy()

	def on_btnOk_clicked(self, *args):
		if self.save_settings():
			self.win.destroy()
		

	def show(self):
		self.win.show()
